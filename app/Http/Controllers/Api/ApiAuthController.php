<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Member;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Config;
use Hash;
use Illuminate\Http\Request as HttpRequest;
use Response;
use Validator;

class ApiAuthController extends Controller
{
		/*
        |-------------------------------------------------------------
        | API Controller
        |-------------------------------------------------------------
        |
        | this controller serve the mobile application as a backend
        | and connected by mobile via API routes
        |
        */

				public function __construct() {

		    }

		    public function index() {
		        return "Auth index";
		    }

		    //
		    // public function authTest(Request $request){
		    //    $token = JWTAuth::getToken();
		    //    $user = JWTAuth::toUser($token);
		    //
		    //    return Response::json([
		    //        'data' => [
		    //            'id' => $user->id            ]
		    //    ]);
		    //
		    // }

		    protected function createTokenfromUser($user)
		    { Config::set('auth.providers.users.model', Member::class);
		        $token = JWTAuth::fromUser($user);
		        return $token;
		    }
		    protected function createToken($user)
		    { Config::set('auth.providers.users.model', Member::class);

		        $token = JWTAuth::attempt($user);

		        return $token;
		    }
		    /**
		     * Log in with Email and Password.
		     */
		    public function login(Request $request)
		    {
		        $email = $request->input('email');
		        $password = $request->input('password');
		        $user = Member::where('email', '=', $email)->first();
		        if (!$user)
		        {
		            return response()->json(['message' => 'Wrong Email and/or password'], 401);
		        }
		        if (Hash::check($password, $user->password))
		        {
		            $member=['email'=>$email,'password'=>$password];
		            unset($user->password);
		            $profile = json_decode($user->profile);
		            unset($user['profile']);
		            return response()->json(array('token' => $this->createToken($member),'account'=>$user,'profile'=>$profile));

		        }
		        else
		        {
		            return response()->json(['message' => 'Wrong email and/or password'], 401);
		        }
		    }
		    /**
		     * Create Email and Password Account.
		     */
		    public function signup(Request $request)
		    {
		        $messages=[
		            'email.unique'=> 'This email already exists',
		            'mobile.unique'=> 'This mobile already exists',

		        ];
		        $validator = Validator::make($request->all(), [
		            'name' => 'required',
		            'email' => 'required|email|unique:members',
		            'password' => 'required',
		            'mobile' => 'required|unique:members,mobile',
		        ],$messages);
		        if ($validator->fails()) {
		            return response()->json(['message' => $validator->messages()], 400);
		        }
		        $user = new Member();
		        $user->name = $request->input('name');
		        $user->email = $request->input('email');
		        $user->mobile = $request->input('mobile');
		        $user->password = Hash::make($request->input('password'));
		        $user->save();
		        $email = $request->input('email');
		        $password = $request->input('password');
		        $member=['email'=>$email,'password'=>$password];
		        $token = $this->createTokenfromUser($user);
		        unset($user->password);
		        return response()->json(array('token' => $token,'account'=>$user));
		        }
		    //forget password API Function
		    public function resetPass (Request $HttpRequest)

		    {       $email = $request->input('email');
		            $password = $request->input('password');
		            $new_password = $request->input('new_password');
		            $user = Member::where('email', '=', $email)->first();
		            if (!$user)
		            {
		                return response()->json(['message' => 'Wrong Email and/or password'], 401);
		            }
		            if (Hash::check($password, $user->password))
		            {
		                $user->password = Hash::make($new_password);
		                $user->save();
		                unset($user->password);
		                return response()->json(['message' => 'Password changed'], 201);
		            }
		            else
		            {
		                return response()->json(['message' => 'Wrong email and/or password'], 401);
		            }

		    }

}
