<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use App\Member;
use App\Machine;
use App\Device;
use App\Notification;
use DB;

class TCPController extends Controller
{

  public function createMachine(Request $request)
  {

    $machine_id = $request->input('id');
    $setting_flags = $request->input('setting_flags');
    $setting_time = $request->input('setting_time');
    $val_1 = $request->input('val_1');
    $val_2 = $request->input('val_2');
    $password = $request->input('password');
    $timezone = $request->input('timezone');
    $area = $request->input('area');
    $depth = $request->input('depth');
    $admin_1 = $request->input('admin_1');
    $admin_2 = $request->input('admin_2');
    $si = $request->input('si');

    $machine = new Machine;
    $machine->machine_id = $machine_id;
    $machine->setting_time = $setting_time;
    $machine->si = $si;
    $machine->val_1 = $val_1;
    $machine->val_2 = $val_2;
    $machine->setting_flags = $setting_flags;
    $machine->password = $password;
    $machine->area = $area;
    $machine->depth = $depth;
    $machine->admin_1 = $admin_1;
    $machine->admin_2 = $admin_2;
    $machine->save();

    return response()->json($machine,200);
  }


  public function getMachine (Request $request)
  {
    $machine_id = $request->input('id');
    $machine = Machine::where('machine_id',$machine_id)->first();
    if($machine != null)
    {
      return response()->json($machine,200);
    }
    else {
      return response()->json('machine not found!',404);
    }

  }


  public function updateMachine(Request $request)
  {

    $machine_id = $request->input('id');
    $setting_flags = $request->input('setting_flags');
    $setting_time = $request->input('setting_time');
    $val_1 = $request->input('val_1');
    $val_2 = $request->input('val_2');
    if($request->input('admin_1')){
      $password = $request->input('password');
      $timezone = $request->input('timezone');
      $area = $request->input('area');
      $depth = $request->input('depth');
      $admin_1 = $request->input('admin_1');
      $admin_2 = $request->input('admin_2');
      $si = $request->input('si');
    }




    $machine = Machine::where('machine_id',$machine_id)->first();
    if($machine != null)
    {
      $machine->setting_time = $setting_time;
      $machine->si = $si;
      $machine->val_1 = $val_1;
      $machine->val_2 = $val_2;

      $machine->setting_flags = $setting_flags;
      if($request->input('admin_1')){
        $machine->password = $password;
        $machine->area = $area;
        $machine->depth = $depth;
        $machine->admin_1 = $admin_1;
        $machine->admin_2 = $admin_2;
      }
      $machine->save();
      return response()->json($machine,200);
    }
    else {
      return response()->json('machine not found!',404);
    }

  }
public function statusMachine(Request $request)
  {

    $machine_id = $request->input('id');
    $setting_time = $request->input('setting_time');
    $val_1 = $request->input('val_1');
    $val_2 = $request->input('val_2');
    $si = $request->input('si');
    $status_flags = $request->input('status_flags');




    $machine = Machine::where('machine_id',$machine_id)->first();
    if($machine != null)
    {
      $machine->setting_time = $setting_time;
      $machine->si = $si;
      $machine->val_1 = $val_1;
      $machine->val_2 = $val_2;
      $machine->status_flags = $status_flags;
      $machine->save();
      
      $over_flow = substr($status_flags,0,1);
      $under_flow = substr($status_flags,1,1);
      $floater_or_pressure = substr($status_flags,2,1);
      $tamam_connection = substr($status_flags,3,1);

      $tamam_indicator_1 = substr($status_flags,4,1);
      $tamam_indicator_2 = substr($status_flags,5,1);
      $tamam_indicator_3 = substr($status_flags,6,1);
      $tamam_indicator_4 = substr($status_flags,7,1);

      return response()->json($machine,200);
    }

  }



}
