<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as HttpRequest;
use App\Machine;
use Excel;

class MachineController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    public function index() {
      $machines = Machine::paginate(15);
      return view('machines', compact('machines'));
    }

    public function addMachine(HttpRequest $HttpRequest)
    {
        $machine = new Machine();
        $machine->machine_id = $HttpRequest['machine_id'];
        $machine->name = $HttpRequest['name'];
        $machine->mobile = $HttpRequest['mobile'];
        $machine->password = $HttpRequest['password'];
        $machine->serial = $HttpRequest['serial'];
        $machine->type = $HttpRequest['type'];
        $machine->admin_1 = $HttpRequest['admin_1'];
        $machine->admin_2 = $HttpRequest['admin_2'];
        $machine->save();
        return view('machines',['message' => 'Machine Added']);
    }


    public function getMachine (HttpRequest $HttpRequest)
    {
        $machine = Machine::where('machine_id',$machine_id);
        if ($machine->first())
        {
          return view('machine',['machine' => $machine]);
        }
        else {
          return view('machines',['message' => 'Machine Not Found']);
        }
    }
    public function getMachineById ($id)
    {
        $machine = Machine::find($id);
        return view('machine',['machine' => $machine]);
    }
    public function editMachine($id)
    {
        $machine = Machine::find($id);
        $machine->id = $HttpRequest['machine_id'];
        $machine->name = $HttpRequest['name'];
        $machine->mobile = $HttpRequest['mobile'];
        $machine->password = $HttpRequest['password'];
        $machine->serial = $HttpRequest['serial'];
        $machine->type = $HttpRequest['type'];
        $machine->admin_1 = $HttpRequest['admin_1'];
        $machine->admin_2 = $HttpRequest['admin_2'];
        $machine->save();
        return view('machine',['machine' => $machine]);
    }

    public function deleteMachine($id) {
        Machine::destroy($id);
        return view('machines',['message' => 'Machine Deleted']);

    }
    public function exportExcel()
   {
       $machines = Machine::all();
       Excel::create('machines', function($excel) use($machines) {
       $excel->sheet('machines sheet', function($sheet) use($machines)
       {
           $sheet->fromArray($machines);
       });
       })->download('xls');

   }


}
