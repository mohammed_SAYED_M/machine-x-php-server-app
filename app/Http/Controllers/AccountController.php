<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as HttpRequest;
use App\User;
use Hash;

class AccountController extends Controller
{
  public function __construct() {
      $this->middleware('auth');
  }

  public function index() {
      $users = User::all();
      return view('accounts', ['users' => $users]);
  }


  public function addAccount(HttpRequest $HttpRequest) {

      $account = new User();
      $account->email = $HttpRequest['email'];
      $account->name = $HttpRequest['name'];
      $account->password = Hash::make($HttpRequest['password']);
      $account->save();
      $users = User::all();
      return view('accounts', ['users' => $users]);

  }

  public function getAccount($id) {

      $Account = User::findOrFail($id);

      return view('account', ['account' => $Account]);
  }

  public function editAccount($id, HttpRequest $HttpRequest) {

      $Account = User::findOrFail($id);
      $Account->password = Hash::make($HttpRequest['password']);

      $Account->save();

      return view('account', ['account' => $Account]);
  }

  public function deleteAccount($id) {
      User::destroy($id);
      $url="/accounts";
      return redirect($url);

  }


}
