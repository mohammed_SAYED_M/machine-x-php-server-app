<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as HttpRequest;
use App\Member;
use Hash;

class MemberController extends Controller
{
  public function __construct() {
      $this->middleware('auth');
  }
  public function index() {

    return view('members');
  }


  public function getMember (HttpRequest $HttpRequest)
  {
      $member = Member::findOrFail($HttpRequest['id']);
      return view('member',['member' => $member]);
  }

  public function editMember($id)
  {
      $member = Member::findOrFail($id);
      $member->name = $HttpRequest['name'];
      $member->mobile = $HttpRequest['mobile'];
      $member->verified = $HttpRequest['verified'];
      $member->save();
      return view('member',['member' => $member]);
  }





}
