@extends('layouts.app')

@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script>
    $(document).ready(function(){

        $("#create").click(function(){
            $("#searchbox").hide(1000);
            $("#listbox").hide(1000);

            $("#createbox").toggle(1000);
        });
        $("#list").click(function(){
            $("#createbox").hide(1000);
            $("#searchbox").hide(1000);

            $("#listbox").toggle(1000);
        });

    });
</script>
@if (isset($message))
<h3 style="color:green;">{{ $message }}</h1>
@endif

                  <h1>Machines Management</h1>
                  <div class="row">
                      <div class="col col-lg-3">
                          <button id="create" class="btn btn-primary">Create</button>
                          <button id="list" class="btn btn-primary">Get Machine by ID</button>
                      </div>
                      <div class="col col-lg-3">

                          <a href="../exportexcel" class="btn btn-danger" role="button">Export Excel</a>
                      </div>

                  </div>



                  <div class="box box-primary" id="createbox"  style="display:none">
                      <h2 class="box-title">Create Machine</h2>
                      <form role="form" method="POST" action="addmachine">
                          {!! csrf_field() !!}
                          <div class="form-group">
                              <label for="id">id:</label>
                              <input class="form-control" id="id"  name="id"  placeholder="ID (integer)">
                          </div>
                          <div class="form-group">
                              <label for="name">Name:</label>
                              <input class="form-control" id="name"  name="name"  placeholder="Name">
                          </div>
                          <div class="form-group">
                              <label for="mobile">Mobile:</label>
                              <input class="form-control" id="mobile"  name="mobile"  placeholder="mobile">
                          </div>

                          <div class="form-group">
                              <label for="password">Password:</label>
                              <input class="form-control" id="password"  name="password"  placeholder="password">
                          </div>
                          <div class="form-group">
                              <label for="serial">Serial:</label>
                              <input class="form-control" id="serial"  name="serial"  placeholder="serial">
                          </div>

                          <div class="form-group">
                              <label for="type">Type:</label>
                              <input class="form-control" id="type"  name="type"  placeholder="type">
                          </div>
                          <div class="form-group">
                              <label for="admin_1">Admin Mobile 1:</label>
                              <input class="form-control" id="admin_1"  name="admin_1"  placeholder="admin mobile 1">
                          </div>
                          <div class="form-group">
                              <label for="admin_2">Admin Mobile 2:</label>
                              <input class="form-control" id="admin_2"  name="admin_2"  placeholder="admin mobile 2">
                          </div>
                          <div class="box-footer">


                              <button type="submit" class="btn btn-primary">Create</button>
                          </div>
                      </form>
                  </div>
                  <div class="box box-primary" id="listbox" style="display:none" >
                      <h2>Search</h2>

                      <form role="form" method="GET" action="getmachine">
                          {!! csrf_field() !!}

                          <div class="form-group">
                              <label for="name">ID:</label>
                              <input class="form-control" id="id"  name="id"  placeholder="machine id">
                          </div>


                          <div class="box-footer">


                              <button type="submit" class="btn btn-primary">Get Machine</button>
                          </div>
                      </form>

                  </div>
                  @if (isset($machines))

                  <h2>All Machines</h2>
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>Machine ID</th>
                        <th>Name</th>
                        <th>Mobile</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($machines as $machine)

                      <tr>
                        <td><a href="getmachinebyid/{{ $machine->id }}">{{ $machine->machine_id }}</a></td>
                        <td>{{$machine->name}}</td>
                        <td>{{$machine->mobile}}</td>
                      </tr>
                      @endforeach


                    </tbody>
                  </table>
                  <?php echo $machines->render(); ?>
                  @endif
                </div>

@endsection
