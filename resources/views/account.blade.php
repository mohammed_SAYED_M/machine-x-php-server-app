@extends('layouts.app')

@section('content')


                  <h1>Edit Account</h1>

                  <div class="box box-primary">
                      <div class="box-header with-border">
                          <h3 class="box-title">Account # {{ $account->id }}</h3>
                      </div>
                      <!-- /.box-header -->
                      <!-- form start -->
                      <form role="form" method="POST" action="../editaccount/{{ $account->id }}">
                                                  {!! csrf_field() !!}

                          <div class="box-body">

                              <div class="form-group">
                                  <label >Name:  {{$account->name}} </label>

                              </div>

                              <div class="form-group">

                                  <label >Email: {{$account->email}} </label>

                              </div>

                              <div class="form-group">
                                  <label for="password">Password:</label>
                                  <input class="form-control" id="password"  name="password"  placeholder="New Password" >
                              </div>

                          </div>
                  <!--         /.box-body -->


                          <div class="box-footer">
                              <button type="submit" class="btn btn-primary">Edit</button>
                          </div>
                      </form>

                      <a href="../deleteaccount/{{$account->id}}" class="btn btn-danger" role="button">Delete Account</a>

                  </div>


@endsection
