@extends('layouts.app')

@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script>
    $(document).ready(function(){

        $("#create").click(function(){
            $("#searchbox").hide(1000);
            $("#listbox").hide(1000);

            $("#createbox").toggle(1000);
        });
        $("#list").click(function(){
            $("#createbox").hide(1000);
            $("#searchbox").hide(1000);

            $("#listbox").toggle(1000);
        });

    });
</script>

                  <h1>Accounts Management</h1>
                  <div class="row">
                      <div class="col col-lg-3">

                          <button id="create" class="btn btn-primary">Create</button>
                          <button id="list" class="btn btn-primary">List</button>

                      </div >

                  </div>



                  <div class="box box-primary" id="createbox"  style="display:none">
                      <h2 class="box-title">Create Account</h2>
                      <form role="form" method="POST" action="addaccount">
                          {!! csrf_field() !!}

                          <div class="form-group">
                              <label for="name">Name:</label>
                              <input class="form-control" id="name"  name="name"  placeholder="Name">
                          </div>
                          <div class="form-group">
                              <label for="email">Email:</label>
                              <input class="form-control" id="email"  name="email"  placeholder="Email">
                          </div>

                          <div class="form-group">
                              <label for="password">Password:</label>
                              <input class="form-control" id="password"  name="password"  placeholder="password">
                          </div>

                          <div class="box-footer">


                              <button type="submit" class="btn btn-primary">Create</button>
                          </div>
                      </form>
                  </div>
                  <div class="box box-primary" id="listbox"  >
                      <h2>Accounts list</h2>

                      <!-- /.box-header -->
                      <div class="box-body">
                          <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                              <div class="row">
                                  <div class="col-sm-6">
                                      <div class="dataTables_length" id="example1_length">
                                      </div></div><div class="col-sm-6">
                                      <div id="example1_filter" class="dataTables_filter">
                                      </div></div></div><div class="row"><div class="col-sm-12"><table id="example1" class="table table-bordered table-striped dataTable hover" role="grid" aria-describedby="example1_info">
                                          <thead>
                                          <tr role="row">
                                              <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending"style="width: 30px;">ID</th>
                                              <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  style="width: 100px;">Name</th>
                                          </tr>
                                          </thead>
                                          <tbody>
                                          @foreach ($users as $user)
                                          <tr role="row" class="odd">
                                              <td class="sorting_1"><a href="getaccount/{{ $user->id }}">{{ $user->id }}</a></td>
                                              <td>{{ $user->name }}</td>
                                          </tr>
                                          @endforeach

                                          </tbody>
                                          <tfoot>

                                          <tr>
                                              <th  rowspan="1" colspan="1">ID</th>
                                              <th rowspan="1" colspan="1">Name</th>
                                          </tr>
                                          </tfoot>
                                      </table></div></div></div>
                      </div>
                      <!-- /.box-body -->
                  </div>

                </div>
@endsection
