@extends('layouts.app')

@section('content')

                  <h1>Edit Machine</h1>

                  <div class="box box-primary">
                      <div class="box-header with-border">
                          <h3 class="box-title">Machine # {{ $machine->id }}</h3>
                      </div>
                      <!-- /.box-header -->
                      <!-- form start -->
                      <form role="form" method="POST" action="../editmachine/{{ $machine->id }}">
                                                  {!! csrf_field() !!}

                          <div class="box-body">
                            <div class="form-group">
                                <label >ID:  {{$machine->id}} </label>

                            </div>
                            <div class="form-group">
                                <label >Machine ID:  {{$machine->machine_id}} </label>
                                <input class="form-control" id="machine_id"  name="machine_id"  placeholder="machine_id" value="{{$machine->machine_id}}">

                            </div>
                            <div class="form-group">
                                <label >Name:  {{$machine->name}} </label>
                                <input class="form-control" id="name"  name="name"  placeholder="name" value="{{$machine->name}}">

                            </div>

                            <div class="form-group">
                                <label >Mobile:  {{$machine->mobile}} </label>
                                <input class="form-control" id="mobile"  name="mobile"  placeholder="mobile" value="{{$machine->mobile}}">

                            </div>


                              <div class="form-group">
                                  <label >Password:  {{$machine->password}} </label>
                                  <input class="form-control" id="password"  name="password"  placeholder="password" value="{{$machine->password}}">

                              </div>

                              <div class="form-group">

                                  <label >Serial: {{$machine->serial}} </label>
                                  <input class="form-control" id="serial"  name="serial"  placeholder="serial" value="{{$machine->serial}}">

                              </div>

                              <div class="form-group">
                                  <label for="password">Admin Mobile 1: {{$machine->admin_1}}</label>
                                  <input class="form-control" id="admin_1"  name="admin_1"  placeholder="admin mobile 1" value="{{$machine->admin_1}}">
                              </div>
                              <div class="form-group">
                                  <label for="password">Admin Mobile 2: {{$machine->admin_2}}</label>
                                  <input class="form-control" id="admin_2"  name="admin_2"  placeholder="admin mobile 2" value="{{$machine->admin_2}}">
                              </div>


                          </div>
                  <!--         /.box-body -->


                          <div class="box-footer">
                              <button type="submit" class="btn btn-primary">Edit</button>
                          </div>
                      </form>

                      <a href="../deletemachine/{{$machine->id}}" class="btn btn-danger" role="button">Delete Machine</a>

                  </div>
@endsection
