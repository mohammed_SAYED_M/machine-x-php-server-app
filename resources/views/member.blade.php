@extends('layouts.app')

@section('content')


          <h1>Edit Member</h1>

          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Member # {{ $member->id }}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST" action="../editmember/{{ $member->id }}">
              {!! csrf_field() !!}

              <div class="box-body">
                <div class="form-group">
                  <label >ID:  {{$member->id}} </label>
                </div>
                <div class="form-group">
                  <label >Email:  {{$member->email}} </label>
                </div>
                <div class="form-group">
                  <label >Name:  {{$member->name}} </label>
                  <input class="form-control" id="name"  name="name"  placeholder="name" value="{{$member->name}}">

                </div>

                <div class="form-group">
                  <label >Mobile:  {{$member->mobile}} </label>
                  <input class="form-control" id="mobile"  name="mobile"  placeholder="mobile" value="{{$member->mobile}}">

                </div>



                <div class="form-group">

                  <label >Verified: {{$member->verified}} </label>
                  <select class="form-control" name="verified">
                      <option value="{{$member->verified}}" disabled selected>Select status</option>
                      <option value="1">True </option>
                      <option value="0">False</option>

                  </select>
                </div>

              </div>
              <!--         /.box-body -->


              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Edit</button>
              </div>
            </form>

          </div>
@endsection
