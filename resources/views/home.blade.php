@extends('layouts.app')

@section('content')

          @if (!empty($message))
          <ol class="breadcrumb">
            <li class="active">{{$message}}</li>
          </ol>
          @endif
          @if (empty($message))
          <div class="row">
            <div class="col-md-3">
              <div class="info-box">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon bg-blue"><i class="fa fa-child"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Members</span>
                  <span class="info-box-number">{{$member_count}}</span>
                </div>
              </div>
            </div>
            <div class="col-md-3">


              <div class="info-box">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon bg-yellow"><i class="fa fa-star-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Machines</span>
                  <span class="info-box-number">{{$machine_count}}</span>
                </div>
              </div>
            </div>

          </div>
          @endif
@endsection
