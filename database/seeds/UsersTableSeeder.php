<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
          'name' => 'Hesham Watany',
          'email' => 'hesham@machines.com',
          'password' => bcrypt('0123892077'),
      ]);
    }
}
