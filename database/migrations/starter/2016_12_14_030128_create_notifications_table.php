<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run notifications table migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_id')->unsigned();
            $table->integer('machine_id')->unsigned();
            $table->string('type');
            $table->string('title');
            $table->string('detials');
            $table->timestamps();
        });
        Schema::table('notifications', function($table) {
          $table->foreign('member_id')->references('id')->on('members');
          $table->foreign('machine_id')->references('id')->on('machines');
        });
    }

    /**
     * Reverse notifications table migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
