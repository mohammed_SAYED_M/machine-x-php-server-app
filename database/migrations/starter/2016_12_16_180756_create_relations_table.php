<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        Schema::create('relations', function (Blueprint $table) {

      $table->increments('id');
      $table->integer('member_id')->unsigned();
      $table->integer('machine_id')->unsigned();
      $table->timestamps();
    });
    Schema::table('relations', function($table) {
      $table->foreign('member_id')->references('id')->on('members');
      $table->foreign('machine_id')->references('id')->on('machines');
    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
