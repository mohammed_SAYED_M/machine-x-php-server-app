<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run mobile app member table migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('password');
            $table->string('email', 50)->unique();
            $table->string('facebook_id');
            $table->string('mobile',20);
            $table->string('mobile_2',20);
            $table->boolean('verified')->default(False);
            $table->boolean('push_enabled')->default(True);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse mobile app member table migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
