<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMachinesTable extends Migration
{
    /**
     * Run machine table migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machines', function (Blueprint $table) {
            $table->increments('id');
            $table->string('machine_id');
            $table->string('name');
            $table->string('mobile',20);
            $table->string('serial');
            $table->string('password');
            $table->decimal('lat', 9, 6);
            $table->decimal('lng', 9, 6);
            $table->string('type');
            $table->string('setting_time');
            $table->string('val_1');
            $table->string('val_2');
            $table->string('si');
            $table->string('timezone');
            $table->string('area');
            $table->string('depth');
            $table->string('setting_flags');
            $table->string('status_flags');
            $table->string('admin_1',20);
            $table->string('admin_2',20);
            $table->boolean('alert')->default(False);
            $table->string('alert_msg',50);
            $table->dateTime('alert_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse machine table migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('machines');
    }
}
