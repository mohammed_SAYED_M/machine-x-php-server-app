<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'cors',
    'namespace' => 'Api'], function(){
  //  Route::resource('authenticate', 'ApiAuthController', ['only' => ['index']]);
    Route::post('authenticate', 'ApiAuthController@login');
    Route::post('newmember', 'ApiAuthController@signup');
    Route::post('/resetpass', 'ApiAuthController@resetPass');
    Route::post('/tcp/createmachine', 'TCPController@createMachine');
    Route::post('/tcp/updatemachine', 'TCPController@updateMachine');
    Route::post('/tcp/getmachine', 'TCPController@getMachine');
    Route::post('/tcp/updatestatus', 'TCPController@statusMachine');
    

});
Route::group(['middleware' => ['cors'], 'before' => 'jwt-auth',
    'namespace' => 'Api'], function(){
    Route::post('registertoken', 'ApiAuthController@createToken');
    Route::post('updateprofile', 'ApiMemberController@updateProfile');
    Route::post('changepassword', 'ApiAuthController@changePassword');
    Route::post('deletemachine', 'ApiAuthController@deleteMachine');
    Route::get('getmember', 'ApiAuthController@getMember');
    Route::post('getmachine', 'ApiAuthController@getMachine');
    Route::post('updatemachine', 'ApiAuthController@updateMachine');
    Route::post('getusermachines', 'ApiAuthController@getUserMachines');
    Route::post('getnotifications', 'ApiAuthController@getNotifications');

});
