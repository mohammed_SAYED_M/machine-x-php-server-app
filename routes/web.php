<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return 'hello';
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/machines', 'MachineController@index');
Route::post('/addmachine', 'MachineController@addMachine');
Route::get('/getmachine', 'MachineController@getMachine');
Route::get('/getmachinebyid/{id}', 'MachineController@getMachinebyId');
Route::post('/editmachine/{id}', 'MachineController@editMachine');
Route::get('/deletemachine/{id}', 'MachineController@deleteMachine');
Route::get('/exportexcel', 'MachineController@exportExcel');
Route::post('/importexcel', 'MachineController@importExcel');


Route::get('/members', 'MemberController@index');
Route::get('/getmember', 'MemberController@getMember');
Route::post('/editmember/{id}', 'MemberController@editMember');

Route::get('/accounts', 'AccountController@index');
Route::post('/addaccount', 'AccountController@addAccount');
Route::get('/getaccount/{id}', 'AccountController@getAccount');
Route::post('/editaccount/{id}', 'AccountController@editAccount');
Route::get('/deleteaccount/{id}', 'AccountController@deleteAccount');
